<?php
error_reporting(0);
session_start();
if(!isset($_SESSION["ID_USUARIO"])){
	header("Location: index.php");
}
 require("../lib/class.conection.php");
if(isset($_POST["id_cliente_md5"]))
{
     $li_id_cliente  = $_POST["id_cliente_md5"];
     $ls_nombre_cliente = $_POST["nombre_cliente"];
     $ls_razon_social_cliente = $_POST["razon_social_cliente"];
     $ls_puesto_cliente = $_POST["puesto_cliente"];
     $li_telefono_cliente = $_POST["telefono_cliente"];
     $ls_direccion_cliente = $_POST["direccion_cliente"];
     $li_id_usuario_modifica=1;
     $ls_fecha_modifica =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_cliente"=>$li_id_cliente,
        ":nombre_cliente"=>$ls_nombre_cliente,
        ":razon_social_cliente"=>$ls_razon_social_cliente,
        ":puesto_cliente"=>$ls_puesto_cliente,
        ":telefono_cliente"=>$li_telefono_cliente,
        ":direccion_cliente"=>$ls_direccion_cliente,
        ":ID_USUARIO_MODIFICA"=>$li_id_usuario_modifica,
        ":FECHA_MODIFICA"=>$ls_fecha_modifica
    );
    
    $la_datosSalida =  array();

    $ls_script = "UPDATE clientes SET nombre_cliente=:nombre_cliente, razon_social_cliente=:razon_social_cliente, puesto_cliente=:puesto_cliente, telefono_cliente=:telefono_cliente, direccion_cliente=:direccion_cliente, ID_USUARIO_MODIFICA=:ID_USUARIO_MODIFICA, FECHA_MODIFICA=:FECHA_MODIFICA WHERE md5(id_cliente)=:id_cliente ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: clientes.php");
}
if(isset($_POST["id_cliente"]))
{
   
     $li_id_cliente  = $_POST["id_cliente"];
     $ls_nombre_cliente = $_POST["nombre_cliente"];
     $ls_razon_social_cliente = $_POST["razon_social_cliente"];
     $ls_puesto_cliente = $_POST["puesto_cliente"];
     $li_telefono_cliente = $_POST["telefono_cliente"];
     $ls_direccion_cliente = $_POST["direccion_cliente"];
     $li_id_usuario_alta=1;
     $ls_fecha_alta =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_cliente"=>$li_id_cliente,
        ":nombre_cliente"=>$ls_nombre_cliente,
        ":razon_social_cliente"=>$ls_razon_social_cliente,
        ":puesto_cliente"=>$ls_puesto_cliente,
        ":telefono_cliente"=>$li_telefono_cliente,
        ":direccion_cliente"=>$ls_direccion_cliente,
        ":ID_USUARIO_ALTA"=>$li_id_usuario_alta,
        ":FECHA_ALTA"=>$ls_fecha_alta
    );
    
    $la_datosSalida =  array();

    $ls_script = "INSERT INTO clientes(id_cliente,nombre_cliente,razon_social_cliente,
    puesto_cliente,telefono_cliente,direccion_cliente,ID_USUARIO_ALTA,FECHA_ALTA) VALUES(:id_cliente,:nombre_cliente,
    :razon_social_cliente,:puesto_cliente,:telefono_cliente,:direccion_cliente,:ID_USUARIO_ALTA,:FECHA_ALTA); ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: clientes.php");
}
if(isset($_GET["did"])){
    $ls_script = "DELETE FROM clientes where md5(id_cliente)= :id_cliente";
    $la_datosEntrada = array(":id_cliente"=>$_GET["did"]);
    $la_datosCliente =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosCliente, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
    header("Location: clientes.php");
}
if(isset($_GET["id"])){
    $ls_script = "SELECT * FROM clientes where md5(id_cliente)= :id_cliente";
    $la_datosEntrada = array(":id_cliente"=>$_GET["id"]);
    $la_datosCliente =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosCliente, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
}
?>
<html>
 <head>
    <meta charset="UTF-8">
     <title> CLIENTES </title>
    </head>   
    <body>
   <h1>
INGRESE LOS DATOS DEL CLIENTE
</h1>
    <form method="post" action="">

<p> </p>
<fieldset>
        ID de cliente: <?php echo $la_datosCliente[0]["id_cliente"]; ?>
       <input type="hidden" value="<?php echo $_GET["id"]; ?>" name="id_cliente_md5" id="id_cliente_md5">
       <input type="hidden" name="id_cliente" value="<?php echo $la_datosCliente[0]["id_cliente"]; ?>" id="id_cliente">
        
<p> </p>
        Nombre
    <input type="text" required name="nombre_cliente" value="<?php echo $la_datosCliente[0]["nombre_cliente"]; ?>" id="nombre_cliente">

<p> </p>
        Razon Social
        <input type="text" required name="razon_social_cliente" value="<?php echo $la_datosCliente[0]["razon_social_cliente"]; ?>"id="razon_social_cliente">
<p> </p>
        Puesto
        <input type="text" required name="puesto_cliente" value="<?php echo $la_datosCliente[0]["puesto_cliente"]; ?>"id="puesto_cliente">
    <p> </p>
        Telefono
        <input type="text" required name="telefono_cliente" value="<?php echo $la_datosCliente[0]["telefono_cliente"]; ?>"id="telefono_cliente">
    <p> </p>
        Direccion
        <input type="text" required name="direccion_cliente" value="<?php echo $la_datosCliente[0]["direccion_cliente"]; ?>" id="direccion_cliente">
    
    <p><input type="submit" value="Enviar información" > <input type="reset" value="Limpiar formulario"></p>

</fieldset>

        </form>
     <?php

        $ls_script = "SELECT * FROM clientes ORDER BY id_cliente  DESC ";
        $la_datosEntrada = array();
        $la_datosSalida =  array();
        $ls_mensaje = "";

        if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
            echo "Error al recuperar información";
        }

        ?>

        <?php if(count($la_datosSalida) > 0): ?>
            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>ID cliente</th>
                        <th>Nombre Cliente</th>
                        <th>Razon Social</th>
                        <th>Puesto</th>
                        <th>Telefono</th>
                        <th>Direccion</th>
                        <th>Fecha alta</th>
                        <th>Accción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($la_datosSalida as $fila): ?>
                        <tr>
                            <td><?php echo $fila["id_cliente"]; ?></td>
                            <td><?php echo $fila["nombre_cliente"]; ?></td>
                            <td><?php echo $fila["razon_social_cliente"]; ?></td>
                            <td><?php echo $fila["puesto_cliente"]; ?></td>
                            <td><?php echo $fila["telefono_cliente"]; ?></td>
                            <td><?php echo $fila["direccion_cliente"]; ?></td>
                            <td><?php echo $fila["FECHA_ALTA"]; ?></td>
                            <td>
						      <a href="?id=<?php echo md5($fila["id_cliente"]); ?>">Editar</a>
                              <a href="?did=<?php echo md5($fila["id_cliente"]); ?>">Eliminar</a>
					        </td>
                        </tr> 
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <b>No se encontraron datos</b>
        <?php endif; ?>
    
    
    </body>
    
    </html>
    