<?php
error_reporting(0);
session_start();
if(!isset($_SESSION["ID_USUARIO"])){
	header("Location: index.php");
}
 require("../lib/class.conection.php");
if(isset($_POST["id_areas_md5"])){
     $li_id_areas = $_POST["id_areas_md5"];
     $ls_nombre_area = $_POST["nombre_area"];
     $li_id_usuario_modifica=1;
     $ls_fecha_modifica =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_areas"=>$li_id_areas,
        ":nombre_area"=>$ls_nombre_area,
        ":ID_USUARIO_MODIFICA"=>$li_id_usuario_modifica,
        ":FECHA_MODIFICA"=>$ls_fecha_modifica
    );
    
    $la_datosSalida =  array();

    $ls_script = "UPDATE area SET nombre_area=:nombre_area, ID_USUARIO_MODIFICA=:ID_USUARIO_MODIFICA, FECHA_MODIFICA=:FECHA_MODIFICA WHERE md5(id_areas)=:id_areas";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: area.php");
    
}
if(isset($_POST["id_areas"]))
{
   
     $li_id_areas = $_POST["id_areas"];
     $ls_nombre_vendedores = $_POST["nombre_area"];
     $li_id_usuario_alta=1;
     $ls_fecha_alta =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_areas"=>$li_id_areas,
        ":nombre_area"=>$ls_nombre_area,
        ":ID_USUARIO_ALTA"=>$li_id_usuario_alta,
        ":FECHA_ALTA"=>$ls_fecha_alta
    );
    
    $la_datosSalida =  array();

    $ls_script = "INSERT INTO area(id_areas,nombre_area,ID_USUARIO_ALTA,FECHA_ALTA) VALUES(:id_areas,:nombre_area,:ID_USUARIO_ALTA,:FECHA_ALTA); ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: area.php");
}
if(isset($_GET["did"])){
    $ls_script = "DELETE FROM area where md5(id_areas)=:id_areas";
    $la_datosEntrada = array(":id_areas"=>$_GET["did"]);
    $la_datosArea =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosArea, $ls_mensaje) < 0){
       echo "Error al recuperar información";
    }
     header("Location: area.php");
}
if(isset($_GET["id"])){
    $ls_script = "SELECT * FROM area where md5(id_areas)=:id_areas";
    $la_datosEntrada = array(":id_areas"=>$_GET["id"]);
    $la_datosArea =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosArea, $ls_mensaje) < 0){
       echo "Error al recuperar información";
    }
}
?>
<html>
 <head>
    <meta charset="UTF-8">
     <title> AREAS </title>
    </head>   
    <body>
   <h1>
INGRESE LOS DATOS DEL AREA
</h1>
    <form method="post" action="">

<p> </p>
<fieldset>
        ID de Area: <?php echo $la_datosArea[0]["id_areas"]; ?>   
       <input type="hidden" value="<?php echo $_GET["id"]; ?>" name="id_areas_md5" id="id_areas_md5">
       <input type="hidden" name="id_areas" value="<?php echo $la_datosArea[0]["id_areas"]; ?>" id="id_areas">
        
<p> </p>
        Nombre del Area 
    <input type="text" required name="nombre_area" value="<?php echo $la_datosArea[0]["nombre_area"]; ?>" id="nombre_area">

     <p><input type="submit" value="Enviar información" > <input type="reset" value="Limpiar formulario"></p>

</fieldset>

        </form>
    <?php

        $ls_script = "SELECT * FROM area ORDER BY id_areas  DESC ";
        $la_datosEntrada = array();
        $la_datosSalida =  array();
        $ls_mensaje = "";

        if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
            echo "Error al recuperar información";
        }

        ?>

        <?php if(count($la_datosSalida) > 0): ?>
            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>ID Area</th>
                        <th>Nombre </th>
                        <th>Fecha alta</th>
                        <th>Accción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($la_datosSalida as $fila): ?>
                        <tr>
                            <td><?php echo $fila["id_areas"]; ?></td>
                            <td><?php echo $fila["nombre_area"]; ?></td>
                            <td><?php echo $fila["FECHA_ALTA"]; ?></td>
                            <td>
						      <a href="?id=<?php echo md5($fila["id_areas"]); ?>">Editar</a>
                              <a href="?did=<?php echo md5($fila["id_areas"]); ?>">Eliminar</a>
					        </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <b>No se encontraron datos</b>
        <?php endif; ?>
    
        
    
    </body>
    
    </html>
    