<?php
error_reporting(0);
session_start();
if(!isset($_SESSION["ID_USUARIO"])){
	header("Location: index.php");
}
 require("../lib/class.conection.php");
if(isset($_POST["id_proveedor_md5"]))
{

     $li_id_proveedor = $_POST["id_proveedor_md5"];
     $ls_nom_proveedor = $_POST["nombre_proveedor"];
     $ls_razon_social_proveedor = $_POST["razon_social_proveedor"];
     $ls_direccion_proveedor = $_POST["direccion_proveedor"];
     $li_telefono_proveedor = $_POST["telefono_proveedor"];
     $ls_producto_proveedor = $_POST["producto_proveedor"];
     $li_id_usuario_modifica=1;
     $ls_fecha_modifica =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_proveedor"=>$li_id_proveedor,
        ":nombre_proveedor"=>$ls_nom_proveedor,
        ":razon_social_proveedor"=>$ls_razon_social_proveedor,
        ":direccion_proveedor"=>$ls_direccion_proveedor,
        ":telefono_proveedor"=>$li_telefono_proveedor,
        ":producto_proveedor"=>$ls_producto_proveedor,
        ":ID_USUARIO_MODIFICA"=>$li_id_usuario_modifica,
        ":FECHA_MODIFICA"=>$ls_fecha_modifica
    );
    
    $la_datosSalida =  array();

    $ls_script = "UPDATE Proveedores SET nombre_proveedor=:nombre_proveedor,
    razon_social_proveedor=:razon_social_proveedor,
    direccion_proveedor=:direccion_proveedor,
    telefono_proveedor=:telefono_proveedor, producto_proveedor=:producto_proveedor, 
    ID_USUARIO_MODIFICA=:ID_USUARIO_MODIFICA, FECHA_MODIFICA=:FECHA_MODIFICA WHERE md5(id_proveedor)=:id_proveedor";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

   header("Location: Proveedores.php");
}
if(isset($_POST["id_proveedor"]))
{
   
     $li_id_proveedor = $_POST["id_proveedor"];
     $ls_nom_proveedor = $_POST["nombre_proveedor"];
     $ls_razon_social_proveedor = $_POST["razon_social_proveedor"];
     $ls_direccion_proveedor = $_POST["direccion_proveedor"];
     $li_telefono_proveedor = $_POST["telefono_proveedor"];
     $ls_producto_proveedor = $_POST["producto_proveedor"];
     $li_id_usuario_alta=1;
     $ls_fecha_alta =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_proveedor"=>$li_id_proveedor,
        ":nombre_proveedor"=>$ls_nom_proveedor,
        ":razon_social_proveedor"=>$ls_razon_social_proveedor,
        ":direccion_proveedor"=>$ls_direccion_proveedor,
        ":telefono_proveedor"=>$li_telefono_proveedor,
        ":producto_proveedor"=>$ls_producto_proveedor,
        ":ID_USUARIO_ALTA"=>$li_id_usuario_alta,
        ":FECHA_ALTA"=>$ls_fecha_alta
    );
    
    $la_datosSalida =  array();

    $ls_script = "INSERT INTO Proveedores(id_proveedor, nombre_proveedor,
    razon_social_proveedor, direccion_proveedor,telefono_proveedor,
    producto_proveedor,ID_USUARIO_ALTA,FECHA_ALTA) VALUES(:id_proveedor,:nombre_proveedor,
    :razon_social_proveedor,:direccion_proveedor, :telefono_proveedor,:producto_proveedor,
    :ID_USUARIO_ALTA,:FECHA_ALTA); ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: Proveedores.php");
}
if(isset($_GET["did"])){
    $ls_script = "DELETE FROM Proveedores where md5(id_proveedor)= :id_proveedor";
    $la_datosEntrada = array(":id_proveedor"=>$_GET["did"]);
    $la_datosProveedor =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosProveedor, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
    header("Location: Proveedores.php");
}
if(isset($_GET["id"])){
    $ls_script = "SELECT * FROM Proveedores where md5(id_proveedor)= :id_proveedor";
    $la_datosEntrada = array(":id_proveedor"=>$_GET["id"]);
    $la_datosProveedor =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosProveedor, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
}
?>
<html>
 <head>
    <meta charset="UTF-8">
     <title> PROVEEDORES </title>
    </head>   
    <body>
   <h1>
INGRESE LOS DATOS DEL PROVEEDOR
</h1>
    <form method="post" action="">

<p> </p>
<fieldset>
        ID del Proveedor: <?php echo $la_datosProveedor[0]["id_proveedor"]; ?>
       <input type="hidden" value="<?php echo $_GET["id"]; ?>" name="id_proveedor_md5" id="id_proveedor_md5">
       <input type="hidden" name="id_proveedor" value="<?php echo $la_datosProveedor[0]["id_proveedor"]; ?>" id="id_proveedor">
        
<p> </p>
        Nombre
    <input type="text" required name="nombre_proveedor" value="<?php echo $la_datosProveedor[0]["nombre_proveedor"]; ?>" id="nombre_proveedor">

<p> </p>
        Razon Social
        <input type="text" required name="razon_social_proveedor" value="<?php echo $la_datosProveedor[0]["razon_social_proveedor"]; ?>" id="razon_social_proveedor">
    <p> </p>
        Direccion
        <input type="text" required name="direccion_proveedor" value="<?php echo $la_datosProveedor[0]["direccion_proveedor"]; ?>" id="direccion_proveedor">
    <p> </p>
        Telefono
        <input type="text" required name="telefono_proveedor" value="<?php echo $la_datosProveedor[0]["telefono_proveedor"]; ?>" id="telefono_proveedor">
     <p> </p>
        Producto 
        <input type="text" required name="producto_proveedor" value="<?php echo $la_datosProveedor[0]["producto_proveedor"]; ?>"  id="producto_proveedor">
    
    <p><input type="submit" value="Enviar información" > <input type="reset" value="Limpiar formulario"></p>


</fieldset>

        </form>
       <?php

        $ls_script = "SELECT * FROM Proveedores ORDER BY id_proveedor DESC ";
        $la_datosEntrada = array();
        $la_datosSalida =  array();
        $ls_mensaje = "";

        if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
            echo "Error al recuperar información";
        }

        ?>

        <?php if(count($la_datosSalida) > 0): ?>
            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>ID proveedor</th>
                        <th>Nombre Proveedor</th>
                        <th>Razon Social</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Producto</th>
                        <th>Fecha alta</th>
                        <th>Accción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($la_datosSalida as $fila): ?>
                        <tr>
                            <td><?php echo $fila["id_proveedor"]; ?></td>
                            <td><?php echo $fila["nombre_proveedor"]; ?></td>
                            <td><?php echo $fila["razon_social_proveedor"]; ?></td>
                            <td><?php echo $fila["direccion_proveedor"]; ?></td>
                            <td><?php echo $fila["telefono_proveedor"]; ?></td>
                            <td><?php echo $fila["producto_proveedor"]; ?></td>
                            <td><?php echo $fila["FECHA_ALTA"]; ?></td>
                            <td>
						      <a href="?id=<?php echo md5($fila["id_proveedor"]); ?>">Editar</a>
                              <a href="?did=<?php echo md5($fila["id_proveedor"]); ?>">Eliminar</a>
					        </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <b>No se encontraron datos</b>
        <?php endif; ?>
    
    </body>
    
    </html>
    