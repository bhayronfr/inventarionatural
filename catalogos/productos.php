<?php
error_reporting(0);
session_start();
if(!isset($_SESSION["ID_USUARIO"])){
	header("Location: index.php");
}
 require("../lib/class.conection.php");
 require("../componentes/listas_desplegables.php");
if(isset($_POST["id_producto_md5"]))
{
     $li_id_producto = $_POST["id_producto_md5"];
     $li_id_proveedor = $_POST["id_proveedor"];
     $ls_nombre_productos = $_POST["nombre_productos"];
     $ls_descripcion_producto = $_POST["descripcion_producto"];
     $li_lote_producto = $_POST["lote_producto"];
     $ls_caducidad_producto = $_POST["caducidad_producto"];
     $li_codigo_de_barras_producto = $_POST["codigo_de_barras_producto"];
     $li_max_producto = $_POST["max_producto"];
     $li_min_producto = $_POST["min_producto"];
     $li_id_usuario_modifica=1;
     $ls_fecha_modifica =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_producto"=>$li_id_producto,
        ":id_proveedor"=>$li_id_proveedor,
        ":nombre_productos"=>$ls_nombre_productos,
        ":descripcion_producto"=>$ls_descripcion_producto,
        ":lote_producto"=>$li_lote_producto,
        ":caducidad_producto"=>$ls_caducidad_producto,
        ":codigo_de_barras_producto"=>$li_codigo_de_barras_producto,
        ":max_producto"=>$li_max_producto,
        ":min_producto"=>$li_min_producto,
        ":ID_USUARIO_MODIFICA"=>$li_id_usuario_modifica,
        ":FECHA_MODIFICA"=>$ls_fecha_modifica
    );
    
    $la_datosSalida =  array();

    $ls_script = "UPDATE productos SET id_proveedor=:id_proveedor, nombre_productos=:nombre_productos,
    descripcion_producto=:descripcion_producto, lote_producto=:lote_producto, 
    caducidad_producto=:caducidad_producto, codigo_de_barras_producto=:codigo_de_barras_producto,
    max_producto=:max_producto, min_producto=:min_producto, ID_USUARIO_MODIFICA=:ID_USUARIO_MODIFICA, 
    FECHA_MODIFICA=:FECHA_MODIFICA WHERE md5(id_producto)=:id_producto";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: productos.php");
}
if(isset($_POST["id_producto"]))
{
   
     $li_id_producto = $_POST["id_producto"];
     $li_id_proveedor = $_POST["id_proveedor"];
     $ls_nombre_productos = $_POST["nombre_productos"];
     $ls_descripcion_producto = $_POST["descripcion_producto"];
     $li_lote_producto = $_POST["lote_producto"];
     $ls_caducidad_producto = $_POST["caducidad_producto"];
     $li_codigo_de_barras_producto = $_POST["codigo_de_barras_producto"];
     $li_max_producto = $_POST["max_producto"];
     $li_min_producto = $_POST["min_producto"];
     $li_id_usuario_alta=1;
     $ls_fecha_alta =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_producto"=>$li_id_producto,
        ":id_proveedor"=>$li_id_proveedor,
        ":nombre_productos"=>$ls_nombre_productos,
        ":descripcion_producto"=>$ls_descripcion_producto,
        ":lote_producto"=>$li_lote_producto,
        ":caducidad_producto"=>$ls_caducidad_producto,
        ":codigo_de_barras_producto"=>$li_codigo_de_barras_producto,
        ":max_producto"=>$li_max_producto,
        ":min_producto"=>$li_min_producto,
        ":ID_USUARIO_ALTA"=>$li_id_usuario_alta,
        ":FECHA_ALTA"=>$ls_fecha_alta
    );
    
    $la_datosSalida =  array();

    $ls_script = "INSERT INTO productos(id_producto,id_proveedor,nombre_productos,
    descripcion_producto,lote_producto,caducidad_producto,codigo_de_barras_producto,max_producto,
    min_producto,ID_USUARIO_ALTA,FECHA_ALTA) VALUES(:id_producto,:id_proveedor,:nombre_productos,
    :descripcion_producto,:lote_producto,:caducidad_producto,:codigo_de_barras_producto,
    :max_producto,:min_producto,:ID_USUARIO_ALTA,:FECHA_ALTA); ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos: ".$ls_mensaje;
    }

    header("Location: productos.php");
}
if(isset($_GET["did"])){
    $ls_script = "DELETE FROM productos where md5(id_producto)= :id_producto";
    $la_datosEntrada = array(":id_producto"=>$_GET["did"]);
    $la_datosProducto =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosProducto, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
    header("Location: productos.php");
}
if(isset($_GET["id"])){
    $ls_script = "SELECT * FROM productos where md5(id_producto)= :id_producto";
    $la_datosEntrada = array(":id_producto"=>$_GET["id"]);
    $la_datosProducto =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosProducto, $ls_mensaje) < 0){
        echo "Error al recuperar información";
    }
}
$arg_datoIn=array("tipo_lista"=>"proveedores");
$arg_datoProveedores=array();
$arg_mensaje="";

if(f_listasDesplegables($arg_datoIn,$arg_datoProveedores,$arg_mensaje)<0){
  echo $arg_mensaje;  
}

?>
<html>
 <head>
    <meta charset="UTF-8">
     <title> PRODUCTOS </title>
    </head>   
    <body>
   <h1>
INGRESE LOS DATOS DEL PRODUCTO
</h1>
    <form method="post" action="">

<p> </p>
<fieldset>
        ID del producto: <?php echo $la_datosProducto[0]["id_producto"]; ?> 
       <input type="hidden" value="<?php echo $_GET["id"]; ?>"  name="id_producto_md5" id="id_producto_md5">
       <input type="hidden" name="id_producto" value="<?php echo $la_datosProducto[0]["id_producto"]; ?>" id="id_producto">

        
<p> </p>
    
        ID del proveedor 
        <select name="id_proveedor">
        <?php foreach($arg_datoProveedores AS $proveedor): ?>
		<option value="<?php echo $proveedor["id"]; ?>" <?php echo ( ($proveedor["id"] == $la_datosProducto[0]["id_proveedor"])?"selected":"" ) ; ?>><?php echo $proveedor["descripcion"]; ?></option>
	<?php endforeach; ?>
        </select>

<p> </p>
        Nombre
    <input type="text" required name="nombre_productos" value="<?php echo $la_datosProducto[0]["nombre_productos"]; ?>"id="nombre_productos">

<p> </p>
        Descripcion
        <textarea required name="descripcion_producto"  id="descripcion_producto"><?php echo $la_datosProducto[0]["descripcion_producto"]; ?></textarea>
<p> </p>
        Lote
        <input type="text" required name="lote_producto" value="<?php echo $la_datosProducto[0]["lote_producto"]; ?>" id="lote_producto">
    <p> </p>
        Caducidad
        <input type="text" required name="caducidad_producto" value="<?php echo $la_datosProducto[0]["caducidad_producto"]; ?>" id="caducidad_producto">
    <p> </p>
        Codigo de Barras
        <input type="text" required name="codigo_de_barras_producto" value="<?php echo $la_datosProducto[0]["codigo_de_barras_producto"]; ?>" id="codigo_de_barras_producto">
     <p> </p>
        Cantidad Maxima
        <input type="text"required name="max_producto" value="<?php echo $la_datosProducto[0]["max_producto"]; ?>" id="max_producto">
     <p> </p>
        Cantidad Minima
        <input type="text"required name="min_producto" value="<?php echo $la_datosProducto[0]["min_producto"]; ?>" id="min_producto">
     
     <p><input type="submit" value="Enviar información" > <input type="reset" value="Limpiar formulario"></p>

</fieldset>

        </form>
     <?php

        $ls_script = "SELECT productos.id_producto, productos.id_proveedor, productos.nombre_productos,
        productos.descripcion_producto, productos.lote_producto, productos.caducidad_producto, 
        productos.codigo_de_barras_producto, productos.max_producto, productos.min_producto, productos.FECHA_ALTA, 
        Proveedores.nombre_proveedor, Proveedores.nombre_proveedor FROM productos INNER JOIN Proveedores ON 
        (Proveedores.id_proveedor = productos.id_proveedor) ORDER BY id_producto DESC ";
        $la_datosEntrada = array();
        $la_datosSalida =  array();
        $ls_mensaje = "";

        if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
            echo "Error al recuperar información";
        }

        ?>

        <?php if(count($la_datosSalida) > 0): ?>
            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>ID producto</th>
                        <th>ID proveedor </th>
                        <th>Nombre producto</th>
                        <th>Descripcion</th>
                        <th>lote</th>
                        <th>Caducidad</th>
                        <th>Codigo de barras</th>
                        <th>Max</th>
                        <th>Min</th>
                        <th>Fecha alta</th>
                        <th>Accción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($la_datosSalida as $fila): ?>
                        <tr>
                            <td><?php echo $fila["id_producto"]; ?></td>
                            <td><?php echo $fila["nombre_proveedor"]; ?></td>
                            <td><?php echo $fila["nombre_productos"]; ?></td>
                            <td><?php echo $fila["descripcion_producto"]; ?></td>
                            <td><?php echo $fila["lote_producto"]; ?></td>
                            <td><?php echo $fila["caducidad_producto"]; ?></td>
                            <td><?php echo $fila["codigo_de_barras_producto"]; ?></td>
                            <td><?php echo $fila["max_producto"]; ?></td>
                            <td><?php echo $fila["min_producto"]; ?></td>
                            <td><?php echo $fila["FECHA_ALTA"]; ?></td>
                            <td>
						      <a href="?id=<?php echo md5($fila["id_producto"]); ?>">Editar</a>
                              <a href="?did=<?php echo md5($fila["id_producto"]); ?>">Eliminar</a>
					        </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <b>No se encontraron datos</b>
        <?php endif; ?>
    
    
    </body>
    
    </html>
    