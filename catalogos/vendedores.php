<?php
error_reporting(0);
session_start();
if(!isset($_SESSION["ID_USUARIO"])){
	header("Location: index.php");
}
 require("../lib/class.conection.php");
 require("../componentes/listas_desplegables.php");
if(isset($_POST["id_vendedores_md5"]) and isset($_POST["id_vendedores"]))
{
    
     $li_id_vendedores = $_POST["id_vendedores_md5"];
     $ls_nombre_vendedores = $_POST["nombre_vendedores"];
     $li_telefono_vendedores = $_POST["telefono_vendedores"];
     $ls_puesto_vendedores = $_POST["puesto_vendedores"];
     $li_id_area = $_POST["id_area"];
     $li_id_usuario_modifica=1;
     $ls_fecha_modifica =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_vendedores"=>$li_id_vendedores,
        ":nombre_vendedores"=>$ls_nombre_vendedores,
        ":telefono_vendedores"=>$li_telefono_vendedores,
        ":puesto_vendedores"=>$ls_puesto_vendedores,
        ":id_area"=>$li_id_area,
        ":ID_USUARIO_MODIFICA"=>$li_id_usuario_modifica,
        ":FECHA_MODIFICA"=>$ls_fecha_modifica
    );
    
    $la_datosSalida =  array();

    $ls_script = "UPDATE vendedores SET nombre_vendedores=:nombre_vendedores,
    telefono_vendedores=:telefono_vendedores, puesto_vendedores=:puesto_vendedores,
    id_area=:id_area, ID_USUARIO_MODIFICA=:ID_USUARIO_MODIFICA, FECHA_MODIFICA=:FECHA_MODIFICA WHERE
    md5(id_vendedores)=:id_vendedores";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos UPDATE: ".$ls_mensaje;
    }

   header("Location: vendedores.php");
}
if(isset($_POST["id_vendedores"]) and !isset($_POST["id_vendedores_md5"]))
{
   
     $li_id_vendedores = $_POST["id_vendedores"];
     $ls_nombre_vendedores = $_POST["nombre_vendedores"];
     $li_telefono_vendedores = $_POST["telefono_vendedores"];
     $ls_puesto_vendedores = $_POST["puesto_vendedores"];
     $li_id_area = $_POST["id_area"];
     $li_id_usuario_alta=1;
     $ls_fecha_alta =date("Y-m-d H:i:s");
    
    $la_datosEntrada=array(
        ":id_vendedores"=>$li_id_vendedores,
        ":nombre_vendedores"=>$ls_nombre_vendedores,
        ":telefono_vendedores"=>$li_telefono_vendedores,
        ":puesto_vendedores"=>$ls_puesto_vendedores,
        ":id_area"=>$li_id_area,
        ":ID_USUARIO_ALTA"=>$li_id_usuario_alta,
        ":FECHA_ALTA"=>$ls_fecha_alta
    );
    
    $la_datosSalida =  array();

    $ls_script = "INSERT INTO vendedores(id_vendedores,nombre_vendedores,telefono_vendedores,
    puesto_vendedores,id_area,ID_USUARIO_ALTA,FECHA_ALTA) 
    VALUES(:id_vendedores,:nombre_vendedores,:telefono_vendedores,:puesto_vendedores,
    :id_area,:ID_USUARIO_ALTA,:FECHA_ALTA); ";
    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
        echo "Error al guardar datos insert: ".$ls_mensaje;
    }

  header("Location: vendedores.php");
}
if(isset($_GET["did"])){
    $ls_script = "DELETE FROM vendedores where md5(id_vendedores)=:id_vendedores";
    $la_datosEntrada = array(":id_vendedores"=>$_GET["did"]);
    $la_datosVendedores = array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosVendedores, $ls_mensaje) < 0){
       echo "Error al recuperar información";
    }
      header("Location: vendedores.php");
}

if(isset($_GET["id"])){
    $ls_script = "SELECT * FROM vendedores where md5(id_vendedores)=:id_vendedores";
    $la_datosEntrada = array(":id_vendedores"=>$_GET["id"]);
    $la_datosVendedores = array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosVendedores, $ls_mensaje) < 0){
       echo "Error al recuperar información";
    }
}

$arg_datoIn=array("tipo_lista"=>"area");
$arg_datoArea=array();
$arg_mensaje="";

if(f_listasDesplegables($arg_datoIn,$arg_datoArea,$arg_mensaje)<0){
  echo $arg_mensaje;  
}

?>
<html>
 <head>
    <meta charset="UTF-8">
     <title> VENDEDORES </title>
    </head>   
    <body>
   <h1>
INGRESE LOS DATOS DEL VENDEDOR
</h1>
    <form method="post" action="">

<p> </p>
<fieldset>
        ID de vendedor: <?php echo $la_datosVendedores[0]["id_vendedores"]; ?> 
       <input type="hidden" value="<?php echo $_GET["id"]; ?>" name="id_vendedores_md5" id="id_vendedores_md5">
       <input type="hidden" name="id_vendedores" value="<?php echo $la_datosVendedores[0]["id_vendedores"]; ?>" id="id_vendedores">
        
<p> </p>
        Nombre del Vendedor 
    <input type="text" required name="nombre_vendedores" value="<?php echo $la_datosVendedores[0]["nombre_vendedores"]; ?>" id="nombre_vendedores">

<p> </p>
        Telefono del Vendedor
        <input type="text" required name="telefono_vendedores" value="<?php echo $la_datosVendedores[0]["telefono_vendedores"]; ?>" id="telefono_vendedores">
<p> </p>
        Puesto del Vendedor
        <input type="text" required name="puesto_vendedores" value="<?php echo $la_datosVendedores[0]["puesto_vendedores"]; ?>" id="puesto_vendedores">
    <p> </p>
        ID de Area
        <select name="id_area">
        <?php foreach($arg_datoArea AS $area): ?>
		<option value="<?php echo $area["id"]; ?>" <?php echo ( ($area["id"] == $la_datosVendedores[0]["id_area"])?"selected":"" ) ; ?>><?php echo $area["descripcion"]; ?></option>
	<?php endforeach; ?>
        </select>
    
     <p><input type="submit" value="Enviar información" > <input type="reset" value="Limpiar formulario"></p>

</fieldset>

        </form>
    <?php
        $ls_script = "SELECT vendedores.id_vendedores, vendedores.nombre_vendedores, vendedores.telefono_vendedores,
        vendedores.puesto_vendedores, vendedores.id_area, vendedores.FECHA_ALTA, area.nombre_area
        FROM vendedores INNER JOIN area ON (area.id_areas = vendedores.id_area) ORDER BY vendedores.id_vendedores  DESC ";
        $la_datosEntrada = array();
        $la_datosSalida =  array();
        $ls_mensaje = "";

        if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
            echo "Error al recuperar información";
        }

        ?>

        <?php if(count($la_datosSalida) > 0): ?>
            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>ID Vendedor</th>
                        <th>Nombre </th>
                        <th>Telefono</th>
                        <th>Puesto</th>
                        <th>Area</th>
                        <th>Fecha alta</th>
                        <th>Accción</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($la_datosSalida as $fila): ?>
                        <tr>
                            <td><?php echo $fila["id_vendedores"]; ?></td>
                            <td><?php echo $fila["nombre_vendedores"]; ?></td>
                            <td><?php echo $fila["telefono_vendedores"]; ?></td>
                            <td><?php echo $fila["puesto_vendedores"]; ?></td>
                            <td><?php echo $fila["nombre_area"]; ?></td>
                            <td><?php echo $fila["FECHA_ALTA"]; ?></td>
                            <td>
						      <a href="?id=<?php echo md5($fila["id_vendedores"]); ?>">Editar</a> 
                              <a href="?did=<?php echo md5($fila["id_vendedores"]); ?>">Eliminar</a> 
					        </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <b>No se encontraron datos</b>
        <?php endif; ?>
    
        
    
    </body>
    
    </html>
    