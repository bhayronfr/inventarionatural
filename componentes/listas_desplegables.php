<?php
function f_listasDesplegables($arg_datoIn=array(),&$arg_datoOut=array(),&$arg_mensaje=""){
    $ls_script = "";
    if ($arg_datoIn["tipo_lista"]=="proveedores"){
      $ls_script = " SELECT id_proveedor AS id, nombre_proveedor AS descripcion FROM proveedores";
    }
       if ($arg_datoIn["tipo_lista"]=="area"){
      $ls_script = " SELECT id_areas AS id, nombre_area AS descripcion FROM area";
    }
    $la_datosEntrada = array();
    $la_datosSalida =  array();
    $ls_mensaje = "";

    if(f_SQL($ls_script, $la_datosEntrada, $la_datosSalida, $ls_mensaje) < 0){
       $ls_mensaje= "Error al recuperar información";
        return -1;
    }
    $arg_datoOut=$la_datosSalida;
    return 1;
}

?>